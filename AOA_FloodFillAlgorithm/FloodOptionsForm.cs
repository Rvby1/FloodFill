﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;

namespace AOA_FloodFillAlgorithm
{
    public partial class FloodOptionsForm : Form
    {
        public ImagesToFloodForm imageToFloodForm; //set by imageToFloodForm
        public int Threshold { get { return (int)Math.Floor(num_threshold.Value); } } //take the floor of the threshold value to ensure it's an integer

        public FloodOptionsForm()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //set up image open dialog
            dialog_imageOpen.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            dialog_imageOpen.Title = "Open Image";
            dialog_imageOpen.FileName = "";
            dialog_imageOpen.Filter = "PNG Images|*.png|JPEG Images|*.jpg|GIF Images|*.gif|BITMAPS|*.bmp";

            //set up image save dialog
            dialog_imageSave.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            dialog_imageSave.Title = "Save Image";
            dialog_imageSave.FileName = "";
            dialog_imageSave.Filter = "PNG Images|*.png|JPEG Images|*.jpg|GIF Images|*.gif|BITMAPS|*.bmp";
        }

        private void btn_changeFillColor_Click(object sender, EventArgs e)
        {
            //show a dialog to select a fill color
            DialogResult colorChoice = dialog_fillColor.ShowDialog();

            if (colorChoice == DialogResult.OK)
            {
                imageToFloodForm.fillColor = dialog_fillColor.Color.ToArgb();
            }

            //display the current fill color
            lbl_fillColor.Text = "Current Fill Color: " + 
                ColorTranslator.ToHtml(dialog_fillColor.Color).ToString();
        }

        private void btn_openImage_Click(object sender, EventArgs e)
        {
            DialogResult imageChoice = dialog_imageOpen.ShowDialog();

            if(imageChoice == DialogResult.OK)
            {
                if(dialog_imageOpen.CheckPathExists)
                {
                    imageToFloodForm.SetFloodPictureBoxImage(Image.FromFile(dialog_imageOpen.FileName));
                }
            }
        }

        private void btn_saveImage_Click(object sender, EventArgs e)
        {
            Image imageToSave = imageToFloodForm.GetFloodPictureBoxImage();
            //do nothing if there isn't an image to save 
            if (imageToSave == null)
            {
                MessageBox.Show("You need to have an image before you can save one!", "Error: No Image");
                return;
            }

            DialogResult imageSave = dialog_imageSave.ShowDialog();
            if (imageSave == DialogResult.OK)
            {
                string extension = Path.GetExtension(dialog_imageSave.FileName);

                if(dialog_imageSave.CheckPathExists)
                {
                    switch (extension.ToLower())
                    {
                        case ".png":
                            imageToSave.Save(dialog_imageSave.FileName, ImageFormat.Png);
                            break;
                        case ".jpg":
                            imageToSave.Save(dialog_imageSave.FileName, ImageFormat.Jpeg);
                            break;
                        case ".gif":
                            imageToSave.Save(dialog_imageSave.FileName, ImageFormat.Gif);
                            break;
                        case ".bmp":
                            imageToSave.Save(dialog_imageSave.FileName, ImageFormat.Bmp);
                            break;
                        default:
                            MessageBox.Show("Sorry! We only support saving in .png, .jpg, .gif, and .bmp formats.", 
                                "Choose Another Format");
                            break;
                    }
                }
            }
        }
    }
}
