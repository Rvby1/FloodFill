﻿namespace AOA_FloodFillAlgorithm
{
    partial class FloodOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.num_threshold = new System.Windows.Forms.NumericUpDown();
            this.btn_changeFillColor = new System.Windows.Forms.Button();
            this.lbl_fillColor = new System.Windows.Forms.Label();
            this.dialog_fillColor = new System.Windows.Forms.ColorDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_saveImage = new System.Windows.Forms.Button();
            this.btn_selectImage = new System.Windows.Forms.Button();
            this.dialog_imageOpen = new System.Windows.Forms.OpenFileDialog();
            this.dialog_imageSave = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_threshold)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.num_threshold);
            this.groupBox1.Controls.Add(this.btn_changeFillColor);
            this.groupBox1.Controls.Add(this.lbl_fillColor);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(192, 129);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Threshold:";
            // 
            // num_threshold
            // 
            this.num_threshold.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num_threshold.Location = new System.Drawing.Point(93, 93);
            this.num_threshold.Name = "num_threshold";
            this.num_threshold.Size = new System.Drawing.Size(81, 25);
            this.num_threshold.TabIndex = 3;
            this.num_threshold.ThousandsSeparator = true;
            this.num_threshold.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btn_changeFillColor
            // 
            this.btn_changeFillColor.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_changeFillColor.Location = new System.Drawing.Point(6, 21);
            this.btn_changeFillColor.Name = "btn_changeFillColor";
            this.btn_changeFillColor.Size = new System.Drawing.Size(176, 40);
            this.btn_changeFillColor.TabIndex = 1;
            this.btn_changeFillColor.Text = "Select Fill Color";
            this.btn_changeFillColor.UseVisualStyleBackColor = true;
            this.btn_changeFillColor.Click += new System.EventHandler(this.btn_changeFillColor_Click);
            // 
            // lbl_fillColor
            // 
            this.lbl_fillColor.AutoSize = true;
            this.lbl_fillColor.Location = new System.Drawing.Point(14, 64);
            this.lbl_fillColor.Name = "lbl_fillColor";
            this.lbl_fillColor.Size = new System.Drawing.Size(160, 17);
            this.lbl_fillColor.TabIndex = 2;
            this.lbl_fillColor.Text = "Currrent Fill Color: None";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_saveImage);
            this.groupBox2.Controls.Add(this.btn_selectImage);
            this.groupBox2.Location = new System.Drawing.Point(12, 156);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(192, 110);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // btn_saveImage
            // 
            this.btn_saveImage.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_saveImage.Location = new System.Drawing.Point(6, 61);
            this.btn_saveImage.Name = "btn_saveImage";
            this.btn_saveImage.Size = new System.Drawing.Size(175, 43);
            this.btn_saveImage.TabIndex = 1;
            this.btn_saveImage.Text = "Save Image";
            this.btn_saveImage.UseVisualStyleBackColor = true;
            this.btn_saveImage.Click += new System.EventHandler(this.btn_saveImage_Click);
            // 
            // btn_selectImage
            // 
            this.btn_selectImage.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_selectImage.Location = new System.Drawing.Point(6, 12);
            this.btn_selectImage.Name = "btn_selectImage";
            this.btn_selectImage.Size = new System.Drawing.Size(175, 43);
            this.btn_selectImage.TabIndex = 0;
            this.btn_selectImage.Text = "Select Image";
            this.btn_selectImage.UseVisualStyleBackColor = true;
            this.btn_selectImage.Click += new System.EventHandler(this.btn_openImage_Click);
            // 
            // dialog_imageOpen
            // 
            this.dialog_imageOpen.FileName = "openFileDialog1";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 271);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form2";
            this.ShowIcon = false;
            this.Text = "Options";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_threshold)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_changeFillColor;
        private System.Windows.Forms.Label lbl_fillColor;
        private System.Windows.Forms.ColorDialog dialog_fillColor;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_selectImage;
        private System.Windows.Forms.OpenFileDialog dialog_imageOpen;
        private System.Windows.Forms.Button btn_saveImage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown num_threshold;
        private System.Windows.Forms.SaveFileDialog dialog_imageSave;
    }
}