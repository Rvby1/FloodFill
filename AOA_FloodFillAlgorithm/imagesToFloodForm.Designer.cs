﻿//Author: Daniel Koenig, 2017

namespace AOA_FloodFillAlgorithm
{
    partial class ImagesToFloodForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pb_pictureToFlood = new System.Windows.Forms.PictureBox();
            this.dialog_fillColor = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pictureToFlood)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_pictureToFlood
            // 
            this.pb_pictureToFlood.Location = new System.Drawing.Point(2, 1);
            this.pb_pictureToFlood.Name = "pb_pictureToFlood";
            this.pb_pictureToFlood.Size = new System.Drawing.Size(200, 200);
            this.pb_pictureToFlood.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pb_pictureToFlood.TabIndex = 0;
            this.pb_pictureToFlood.TabStop = false;
            this.pb_pictureToFlood.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pb_pictureToFlood_MouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(208, 207);
            this.Controls.Add(this.pb_pictureToFlood);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image To Flood";
            ((System.ComponentModel.ISupportInitialize)(this.pb_pictureToFlood)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_pictureToFlood;
        private System.Windows.Forms.ColorDialog dialog_fillColor;
    }
}

