﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace AOA_FloodFillAlgorithm
{
    public partial class ImagesToFloodForm : Form
    {
        FloodOptionsForm floodOptionsForm;
        public int fillColor = 0;
        public Image ImageToFlood { get; set; }

        public ImagesToFloodForm()
        {
            InitializeComponent();
            floodOptionsForm = new FloodOptionsForm();
            floodOptionsForm.Show();
            floodOptionsForm.imageToFloodForm = this;

            ImageToFlood = pb_pictureToFlood.Image;
        }

        public void SetFloodPictureBoxImage(Image imageToChangeTo)
        {
            pb_pictureToFlood.Image = imageToChangeTo;
        }

        public Image GetFloodPictureBoxImage()
        {
            return pb_pictureToFlood.Image;
        }

        private void pb_pictureToFlood_MouseClick(object sender, MouseEventArgs e)
        {
            //ensure that there is an image to check:
            if (pb_pictureToFlood.Image == null)
            {
                return;
            }

            Bitmap imageToFlood = (Bitmap)pb_pictureToFlood.Image;
            //ensure that there's a fill color being passed in: 
            if (fillColor != 0)
            {
                FloodFill(imageToFlood, e.Location, fillColor);
            }
            else
            {
                MessageBox.Show("Before you use the flood fill algorithm, you must select a fill color.",
                    "Error: No Fill Color Selected");
            }
        }

        /// <summary>
        /// returns the ARGB color at the given point in the given image
        /// </summary>
        /// <param name="imageToGetColorFrom">the image with the pixel we should check</param>
        /// <param name="pixelToGetColorAt">the point at which we should find the ARGB color</param>
        /// <returns>the ARGB color of the given node</returns>
        private int GetColorAtPoint(Bitmap imageToGetColorFrom, Point pixelToGetColorAt)
        {
            return (imageToGetColorFrom.GetPixel(pixelToGetColorAt.X, pixelToGetColorAt.Y)).ToArgb();
        }

        /// <summary>
        /// sets the color at the specificied point in an image 
        /// </summary>
        /// <param name="imageToSetColorOn">the image where we want to set the pixel</param>
        /// <param name="pointToSetColorAt">point where we want to set the color</param>
        /// <param name="colorToSetPointToARGB">where we should set the color</param>
        private void SetColorAtPoint(Bitmap imageToSetColorOn, Point pointToSetColorAt, int colorToSetPointToARGB)
        {
            imageToSetColorOn.SetPixel(pointToSetColorAt.X, pointToSetColorAt.Y,
                Color.FromArgb(colorToSetPointToARGB));
        }

        /// <summary>
        /// starting at startingPixel, fill all contiguous pixels within a threshold of startingPixel's color with fillColor
        /// </summary>
        /// <param name="imageToFlood">the image that we want to flood</param>
        /// <param name="startingPixel">the pixel we want to start filling at</param>
        /// <param name="fillColor">the color we should flood pixels with</param>
        private void FloodFill(Bitmap imageToFlood, Point startingPixel, int fillColor)
        {
            int colorToFill = GetColorAtPoint(imageToFlood, startingPixel);
            //if the color we want to fill and the color we're filling it with are the same, we shouldn't fill any other pixels 
            if (colorToFill == fillColor)
            {
                return;
            }
            //if the color at our starting pixel isn't the color we want to fill with fillColor, we shouldn't fill any other pixels 
            if (colorToFill != GetColorAtPoint(imageToFlood, startingPixel))
            {
                return;
            }

            //determine the similar colors we should also fill based off of the threshold
            int thresholdMultiplier = 167772; //multiplier to make the threshold functional for large ARGB values
            int adjustedThreshold = floodOptionsForm.Threshold * thresholdMultiplier; 
            int loweradjustedThresholdArgb_colorToFill = colorToFill - adjustedThreshold;
            int upperadjustedThresholdArgb_colorToFill = colorToFill + adjustedThreshold;

            //a queue with all of the points in the image that we need to fill 
            Queue<Point> pointsToFill = new Queue<Point>();
            pointsToFill.Enqueue(startingPixel);

            //a hashset that will hold all of the points we've checked so far 
            HashSet<Point> pointsChecked = new HashSet<Point>();

            //while there are still points to fill... 
            while (pointsToFill.Count != 0)
            {
                Point currentPointToFill = pointsToFill.Dequeue();
                SetColorAtPoint(imageToFlood, currentPointToFill, fillColor);

                //check if the nodes around the current point should be filled:
                int xCoordOfCurrentPoint = currentPointToFill.X;
                int yCoordOfCurrentPoint = currentPointToFill.Y;
                Point[] neighboringPointsToCheck =
                    {
                        new Point(xCoordOfCurrentPoint - 1, yCoordOfCurrentPoint),
                        new Point(xCoordOfCurrentPoint + 1, yCoordOfCurrentPoint),
                        new Point(xCoordOfCurrentPoint, yCoordOfCurrentPoint - 1),
                        new Point(xCoordOfCurrentPoint, yCoordOfCurrentPoint + 1),
                    };

                foreach (Point neighboringPoint in neighboringPointsToCheck)
                {
                    //if we've already checked this point, we can move forward 
                    if (pointsChecked.Contains(neighboringPoint))
                    {
                        continue;
                    }

                    //do a bounds check to ensure point is within the image: 
                    if (neighboringPoint.X < 0 || neighboringPoint.X >= pb_pictureToFlood.Image.Width ||
                        neighboringPoint.Y < 0 || neighboringPoint.Y >= pb_pictureToFlood.Image.Height)
                    {
                        continue;
                    }

                    //check if the point should be filled by seeing if its color is within the threshold of colors we want to fill:
                    int colorAtNeighboringPoint = GetColorAtPoint(imageToFlood, neighboringPoint);
                    if (colorAtNeighboringPoint >= loweradjustedThresholdArgb_colorToFill &&
                        colorAtNeighboringPoint <= upperadjustedThresholdArgb_colorToFill)
                    {
                        pointsToFill.Enqueue(neighboringPoint);
                    }

                    pointsChecked.Add(neighboringPoint);
                }

                pb_pictureToFlood.Refresh();
            }
        }
    }
}
